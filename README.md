# SocialShopping

Ce dépôt contient le code source de :
- la vitrine de e-commerce Faguo (HTML+CSS produits par Selma)
- la popup présentant le contenu social propre à un produit
- le serveur NodeJS

## Installation du projet

1. Cloner le projet : `git clone https://gitlab.com/socialshopping/faguo-store.git`
2. Installer [node.js](https://nodejs.org/en/download/)
2. Démarrer `node.js command prompt` et se déplacer dans le dossier du projet avec la commande `cd`
3. Exécuter la commande `npm install` (permet d'installer les modules complémentaires nécessaires au fonctionnement du projet)
4. Pour démarrer le serveur, utiliser la commande `npm start`

Le serveur démarre alors sur votre machine sur le port 2000. Pour y accéder, connectez-vous avec votre navigateur à l'adresse [http://localhost:2000/](http://localhost:2000/)


## Développement

Pour mieux comprendre la structure du code source du projet, [allez-voir sur le wiki](https://gitlab.com/socialshopping/faguo-store/wikis/home) :smiley:

## Eléments de configuration

### Connexion sécurisée

Sur les versions récentes des navigateurs, l'API `getUserMedia` permettant d'accéder à la webcam de l'utilisateur (scan de code-barre, visioconférence, ...) ne fonctionne que lorsque la connexion est sécurisée (https) ou vers une adresse locale. Il est donc nécessaire de prévoir une connexion sécurisée dès lors que l'on veut démontrer une communication vidéo entre deux PC distincts, celle-ci devant être conifgurée via un reverse proxy.

### Accès aux API des réseaux sociaux

_[Article détaillé](https://gitlab.com/socialshopping/faguo-store/wikis/home)_

L'importation de contenu des réseaux sociaux nécessite l'utilisation de clefs et de token d'applications, celles-ci pouvant être définies de 2 manières :
- via les fichiers de configuration : `config/default.js` ou `config/local.js`
- par des variables d'environnement :
    - FB_APP_ID
    - FP_APP_SECRET
    - TWITTER_BEARER

