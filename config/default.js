var localConfig = {};
try {
    localConfig = require('./local');
} catch (e) {}

module.exports = Object.assign({
    // port d'écoute du serveur
    port: 2000,
    
    // clefs d'API réseaux sociaux
    facebook_appId: process.env['FB_APP_ID'] || 249123778771335,
    facebook_secret: process.env['FB_APP_SECRET'] || '16eba6292bf2f3ed3dc8d02541867cc7',
    twitter_bearer: process.env['TWITTER_BEARER'] || 'AAAAAAAAAAAAAAAAAAAAAEfWxQAAAAAA0wRf2TOvxR3rQ7J1snTAtKuxs%2Fg%3D4bf52trX4vCnMIH1HFdQkFPIdYzHXFhH7CiL6HIxZdAYlLRJQV',
    
    // paramètres mysql
    mysql_host: 'localhost',
    mysql_user: 'root',
    mysql_database: 'socialshopping',
    mysql_password: ''
}, localConfig);