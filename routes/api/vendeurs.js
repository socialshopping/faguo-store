var express = require('express');
var app = express.Router();
var multer  = require('multer');
var crypto = require('crypto');
var path = require('path');

var database = require('../../database');

var vendeurUpload = multer({
    storage: multer.diskStorage({
        destination: 'public/vendeur_profil/',
        filename: function (req, file, cb) {
            crypto.pseudoRandomBytes(16, function (err, raw) {
                if (err) return cb(err)
                cb(null, raw.toString('hex') + path.extname(file.originalname))
            })
        }
    })
});

// Liste des vendeurs
app.get('/', (req, res) => {
    database.listVendeurs()
        .then(vendeurs => res.json(vendeurs))
        .catch(e => res.status(500).send(e));
});

// Ajout d'un vendeur
app.post('/', vendeurUpload.single('image'), (req, res) => {
    req.body.image = req.file.filename;
    database.addVendeur(req.body)
        .then(vendeur => req.query.next ? res.redirect(req.query.next) : res.json(vendeur))
        .catch(e => res.status(500).send(e.stack));
});

// Suppression d'un vendeur
app.delete('/:id', (req, res) => {
    database.deleteVendeur(req.params.id)
        .then(() => res.json({}))
        .catch(e => res.status(500).send(e.stack));
});

// Modification d'un vendeur
app.post('/:id', (req, res) => {
    database.updateVendeur(req.params.id, req.body)
        .then(vendeur => req.query.next ? res.redirect(req.query.next) : res.json(vendeur))
        .catch(e => res.status(500).send(e.stack));
});

module.exports = app;