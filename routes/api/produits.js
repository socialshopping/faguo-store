var express = require('express');
var app = express.Router();
var multer = require('multer');
var crypto = require('crypto');
var path = require('path');

var database = require('../../database');

var productUpload = multer({
    storage: multer.diskStorage({
        destination: 'public/product_image/',
        filename: function (req, file, cb) {
            crypto.pseudoRandomBytes(16, function (err, raw) {
                if (err) return cb(err)
                cb(null, raw.toString('hex') + path.extname(file.originalname))
            })
        }
    })
});

// Liste des produits référencés
app.get('/', (req, res) => {
    database.listProducts()
        .then(produits => res.json(produits))
        .catch(e => res.status(500).send(e));
});

// Détails d'un produit
app.get('/:idProduit', (req, res) => {
    database.getProduct(req.params.idProduit)
        .then(produit => res.json(produit))
        .catch(e => res.status(500).send(e));
});


// Ajout d'un produit à la base de données
app.post('/', productUpload.single('image'), (req, res) => {
    req.body.image = req.file.filename;
    database.addProduct(req.body)
        .then(produit => req.query.next ? res.redirect(req.query.next+'?product_id='+produit.id) : res.json(produit))
        .catch(e => res.status(500).send(e.stack));
});

module.exports = app;