var express = require('express');
var app = express.Router();

app.use('/produits', require('./produits'));
app.use('/vendeur', require('./vendeurs'));
app.use('/questions', require('./questions'));

module.exports = app;