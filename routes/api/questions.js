var express = require('express');
var app = express.Router();

var database = require('../../database');

app.get('/produits/:id', (req, res) => {
    database.listProductQuestions(req.params.id)
        .then(questions => res.json(questions))
        .catch(e => res.status(500).send(e));
});

// Liste des questions
app.get('/', (req, res) => {
    database.listQuestions()
        .then(questions => res.json(questions))
        .catch(e => res.status(500).send(e));
});

app.get('/:id', (req, res) => {
    database.getQuestion(req.params.id)
        .then(question => res.json(question))
        .catch(e => res.status(500).send(e));
});

// Ajout d'une question
app.post('/', (req, res) => {
    database.addQuestion(req.body)
        .then(question => {
            req.app.get('io').sockets.emit('question.new', req.user.nom, question.text, question.id);
            req.query.next ? res.redirect(req.query.next) : res.json(question)
        })
        .catch(e => res.status(500).send(e.stack));
});

app.post('/:idQuestion/reponse', (req, res) => {
    database.addReponse(req.params.idQuestion, req.body)
        .then(question => req.query.next ? res.redirect(req.query.next) : res.json(question))
        .catch(e => res.status(500).send(e.stack));
});

// Suppression d'une question
app.delete('/:id', (req, res) => {
    database.deleteQuestion(req.params.id)
        .then(() => res.json({}))
        .catch(e => res.status(500).send(e.stack));
});

// Modification d'une question
app.post('/:id', (req, res) => {
    database.updateQuestion(req.params.id, req.body)
        .then(question => req.query.next ? res.redirect(req.query.next) : res.json(question))
        .catch(e => res.status(500).send(e.stack));
});

module.exports = app;