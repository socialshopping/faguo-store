var express = require('express');
var app = express.Router();

app.use('/', require('./gestion'));
app.use('/', require('./public'));
app.use('/api', require('./api'));
app.use('/scanner', require('./scanner'));

module.exports = app;