var express = require('express');
var app = express.Router();

var database = require('../database');

app.use((req, res, next) => {
    res.locals.vendeur = req.user;
    next();
});

let requireLogin = (req, res, next) => {
    if (req.user) {
        next();
    } else {
        req.flash('error', 'Vous devez être connecté pour accéder à cet espace.');
        res.redirect('/vendeur/login');
    }
};

app.get('/vendeur', requireLogin, (req, res) => {
    database.listProducts().then(produits => {
        res.render('gestion/vendeur', {
            produits: produits
        });
    }).catch(e => res.status(500).send(e));
});

app.get('/vendeur/login', (req, res, next) => {
    res.render('gestion/login', {
        error: req.flash('error')
    });
});

app.get('/vendeur/logout', (req, res) => {
    req.logout();
    res.redirect('/vendeur');
});

app.get('/vendeur/communication', requireLogin, (req, res) => {
    res.render('gestion/communication');
})

app.get('/vendeur/parametres', requireLogin, (req, res) => {
    res.render('gestion/parametres');
});

app.get('/vendeur/scanner', requireLogin, (req, res) => {
    res.render('gestion/scanner');
});

app.get('/vendeur/questions', requireLogin, (req, res) => {
    Promise.all([
        database.listQuestions(),
        database.listProducts()
    ])
    .then(([questions, produits]) => res.render('gestion/questions', { vendeur: req.user, questions, produits }))
});

app.get('/admin', requireLogin, (req, res) => {
    database.listVendeurs().then(vendeurs => {
        res.render('gestion/admin', {
            vendeurs: vendeurs
        });
    });
});

module.exports = app;