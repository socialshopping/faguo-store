var express = require('express');
var app = express.Router();

var database = require('../database');

// Page d'accueil de la vitrine
app.get('/', (req, res) => {
    database.listProducts().then(produits => {
        res.render('index', {
            produits: produits
        });
    }).catch(e => res.status(500).send(e));
});

// Fiche produit
app.get('/:idProduit', (req, res, next) => {
    if (/produit-(\d+)/.test(req.params.idProduit)) {
        console.log(req.params.idProduit);
        database.getProduct(RegExp.$1).then(produit => {
            res.render('fiche-produit', {
                produit: produit
            });
        }).catch(e => res.status(500).send(e));
    } else next();
});

module.exports = app;