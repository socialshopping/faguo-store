var express = require('express');
var fs = require('fs');
var decoder = require('node-zxing')({});
var app = express.Router();

var database = require('../database');

app.post('/', (req, res) => {
    let data = '';
    req.on('data', chunk => data += chunk);
    req.on('end', () => {
        let buf = new Buffer(data.replace(/^data:image\/\w+;base64,/, ''), 'base64');
        fs.writeFile('./decode.png', buf, () => {
            decoder.decode('./decode.png', function(err, out) {
                if (err) res.status(500).send(err);
                else if (out.length < 1) res.end();
                else database.searchProductBarcode(out)
                    .then(product => res.send(product))
                    .catch(e => res.end(e));
            });
        });
    });
});

module.exports = app;