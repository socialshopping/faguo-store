-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Jeu 18 Mai 2017 à 21:51
-- Version du serveur :  5.7.18-0ubuntu0.16.04.1
-- Version de PHP :  7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `socialshopping`
--

-- --------------------------------------------------------

--
-- Structure de la table `contenu_social`
--

CREATE TABLE `contenu_social` (
  `id` int(11) NOT NULL,
  `type` enum('twitter','facebook','youtube','instagram') NOT NULL,
  `nom_auteur` varchar(255) NOT NULL,
  `r_id` varchar(255) NOT NULL,
  `description` text,
  `media` varchar(255) DEFAULT NULL,
  `ref_produit` int(11) DEFAULT NULL,
  `date_publication` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `contenu_social`
--

INSERT INTO `contenu_social` (`id`, `type`, `nom_auteur`, `r_id`, `description`, `media`, `ref_produit`, `date_publication`) VALUES
(118, 'facebook', 'FAGUO', '', 'Le BAG 48, un sac pour vous suivre jusqu\'au bout du monde (ou presque).\r\nPS : derniers jours de petits prix sur ce modèle bit.ly/2kGfLus\r\n#FAGUOspotted Menlook', 'https://scontent-cdg2-1.xx.fbcdn.net/v/t1.0-0/p480x480/16806980_10154218334120936_5365743585636643425_n.png?oh=1d5877ee75370f3931aa90b3aa0001b1&oe=599AEA21', 17, '2017-02-17 00:00:00'),
(119, 'facebook', 'FAGUO', '88944535935_10153116878645936', 'Bonne nouvelle : les réassorts et nouveautés sur toute la bagagerie sont disponibles sur l\'eshop et en boutique ! \n>> bit.ly/BagagerieFaguo\nProfitez des frais de port gratuits via Colissimo en France !', 'https://scontent.xx.fbcdn.net/v/t31.0-8/s720x720/12052605_10153116875970936_2989504705530735340_o.jpg?oh=2f4e3ed9f7bc6414b9ced977484a9dfb&oe=595DB245', 17, '2015-10-25 18:08:04'),
(120, 'facebook', 'FAGUO', '88944535935_10153206480150936', 'Bonne nouvelle : On a reçu les réassorts en bagagerie sur l\'eshop et à la boutique !  Profitez en sur : bit.ly/BagagerieFaguo', 'https://scontent.xx.fbcdn.net/v/t1.0-9/s720x720/12373218_10153206480150936_3918439766458387435_n.png?oh=a26d7ce8d6e19992b35c779ad7f46156&oe=5969F9D7', 17, '2015-12-18 15:08:39'),
(121, 'facebook', 'FAGUO', '88944535935_10153472145390936', 'On continue notre voyage vers l\'Est ! Une étape de plus pour FAGUO, cette fois-ci en Corée  :) \nLes coréens vous présentent en vidéo nos modèles iconiques : les OAK et les CYPRESS. Nous sommes plus que contents ! Et tout ça c\'est un peu grâce à vous... MERCI !', 'https://scontent.xx.fbcdn.net/v/t15.0-10/s720x720/12419706_10153472156555936_596348200_n.jpg?oh=98a8a939626ee41be3346fd3c60ff678&oe=594F0115', 10, '2016-04-21 10:53:33'),
(123, 'facebook', 'Quitterie Rmt', '88944535935_10154153741300936', 'Hello Faguo ! \nJ\'ai un petit souci. Je suis allée à la vente Arlettie et j\'ai deux chaussures droites Wattle marine en T43. \nEst il possible de faire un changement dans une boutique ?', NULL, 13, '2017-01-24 16:45:40'),
(125, 'facebook', 'FAGUO', '88944535935_10154154020195936', 'Imaginées spécialement pour tous les explorateurs urbains... Vos meilleures alliées pour affronter le froid sont ces sneakers ASPEN. A -30%, elles n\'attendent plus que vous !\nEt pour chaque FAGUO (même en promo), on plante un arbre :)', 'https://scontent.xx.fbcdn.net/v/t1.0-9/s720x720/16143076_10154154020195936_3762439829112828773_n.jpg?oh=a04fb79ca3bdeb838f51884f5e37bcca&oe=59504A5B', 15, '2017-01-24 18:30:01'),
(129, 'twitter', 'FAGUOMX', '778332818216628200', 'Faguo Cypress Tabacco.... Encuéntralos en https://t.co/ruOLhqSd0A https://t.co/YHiWXSpsLD', 'http://pbs.twimg.com/media/Cs0xf3gVMAAPO0u.jpg', 10, '2016-09-20 22:39:39'),
(132, 'twitter', 'magalie renaud', '834881052325908500', 'Super affaire : chaussures en cuir Faguo wattle pour hommes entre 27 et 31€ https://t.co/z6uZNPeNi5 https://t.co/kj1Z42GdNT', 'http://pbs.twimg.com/media/C5YX5BRWYAABd0x.jpg', 13, '2017-02-23 22:42:08'),
(134, 'twitter', 'Chal-Michel', '680814531334004700', 'Faguo Wattle Suede #selfoot #faguo https://t.co/5pQ09exR2x', 'http://pbs.twimg.com/media/CXK9NX_WEAAqAzr.jpg', 14, '2015-12-26 19:16:47'),
(137, 'twitter', 'Guillaume Barre', '821804765961224200', '@FAGUO_FR De rien ! Les Aspen sont juste superbes et terriblement confortables. Je replanterai d\'autres arbres pour… https://t.co/3unV46wrZk', NULL, 15, '2017-01-18 20:41:39'),
(140, 'twitter', 'Le Barboteur', '455940203791024100', 'SAC À DOS FAGUO BAG 24  http://t.co/I7rrKhRmHm #ACCESSOIRES #FAGUO #Sacs #»ONATESTÉ', NULL, 16, '2014-04-15 07:26:30'),
(142, 'twitter', 'Buy2Store', '803817143825473500', 'Faguo – Sac de voyage Bag 48 (bag4801) taille 33 cm https://t.co/X8UxTyV8Sw https://t.co/y1N206nZvy', 'http://pbs.twimg.com/media/Cye7bm6VIAAi_R2.jpg', 17, '2016-11-30 05:25:15'),
(143, 'twitter', 'Buy2Store', '845905242810126300', 'Sac de voyage Faguo Bag 48 Stripes Navy – 53 cm Stripes Navy… https://t.co/D50o66mQjh', NULL, 17, '2017-03-26 09:48:20'),
(150, 'twitter', 'projetshopping', '856161914514268200', 'Moi avec mon sac! \n#Bag48 #bleuDePrusse https://t.co/zAZJMMpvLr', 'http://pbs.twimg.com/media/C-GyLwzWAAAWkby.jpg', 17, '2014-01-23 17:04:41');

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

CREATE TABLE `produit` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `description` text,
  `image` varchar(255) DEFAULT NULL,
  `barcode` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `produit`
--

INSERT INTO `produit` (`id`, `nom`, `type`, `description`, `image`, `barcode`) VALUES
(3, 'ONCA BLEU', 'CHEMISE EN COTON', '', '5d5a4b3d85849330ebb695db688228a8.jpg', NULL),
(4, 'ONCA PORTO', 'CHEMISE EN COTON', '', '6c6188ec75678a4c5fe76c8b3ae402c4.jpg', NULL),
(5, 'ONCA POIS MARINE', 'CHEMISE EN COTON', '', '1686c40efe99e636a14115ad3d6d6e28.jpg', NULL),
(6, 'DARNEY GRIS "J\'AIME MON VELO"', 'SWEAT EN COTON', '', 'd2d053d8bfa89bbbc24daef46d31997d.jpg', NULL),
(7, 'ARCY MARINE "ANTIQUAIRE"', 'TSHIRT EN COTON', '', 'f09232d5e87e71f7405c7127023424eb.jpg', NULL),
(8, 'ARCY GRIS "LES AMOUREUX DU DIMANCHE"', 'TSHIRT EN COTON', '', '257ddfcda728300f3117f9f6386040a8.jpg', NULL),
(9, 'ARCY BLANC "FLANEUSE"', 'TSHIRT EN COTON', '', '54fb2b9bf0ded519664bc73335be17c9.jpg', NULL),
(10, 'CYPRESS ARDOISE ', 'BASKET EN COTON', '', 'e148499c4b9b204129991acef08a7f47.jpg', NULL),
(11, 'SUGI CAMEL ', 'BASKET EN NYLON', '', '45aa178560b5b1026ab3399a79a3c7e2.jpg', NULL),
(12, 'CYPRESS NOIR', 'BASKET EN COTON', '', 'e4a9c41fa595408737dd6a353beb0b4c.jpg', '3700571226171'),
(13, 'WATTLE PORTO', 'BASKET MONTANTE EN DAIM', '', '27695d8a26a5efd4db5d778f6e6cb36a.jpg', NULL),
(14, 'WATTLE GRIS BETON', 'BASKET MONTANTE EN DAIM', '', '21caada5fe7ab4e9526dc0b96743b63b.jpg', NULL),
(15, 'ASPEN BLANC ', 'BASKET MONTANTE EN CUIR ', '', 'cbc2fff9679b084cf487f82da4d30b9a.jpg', NULL),
(16, 'BAG 24 BORDEAUX', 'SAC A DOS EN NYLON', '', '14569b6ee5d813c35f4011e1f268aee7.jpg', NULL),
(17, 'BAG 48 BLEU DE PRUSSE/MARINE', 'SAC WEEKEND EN NYLON', '', '6f9bfcbeebd3d0163053f3054bc2e126.jpg', '3700571148992'),
(18, 'WEEKENDER MARINE', 'SAC WEEKEND EN NYLON', '', '59c76411ed66ac87e3624edda6aad02d.jpg', NULL),
(19, 'WALLET 7 NOIR', 'PORTEFEUILLE EN CUIR', '', '33d4c9d01df51c8d21a7209a773a4fc0.jpg', '3700571220841');

-- --------------------------------------------------------

--
-- Structure de la table `question`
--

CREATE TABLE `question` (
  `id` int(11) NOT NULL,
  `idProduit` int(11) NOT NULL,
  `text` text NOT NULL,
  `idVendeur` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Contenu de la table `question`
--

INSERT INTO `question` (`id`, `idProduit`, `text`, `idVendeur`, `date`) VALUES
(25, 17, 'Quelles sont les consignes de lavage pour ce sac ?', 8, '2017-04-23 19:33:53'),
(26, 17, 'Est-ce qu\'il sera reconduit pour la collection automne/hiver 2017 ?', 9, '2017-04-23 19:40:32'),
(27, 17, 'Quels sont les coloris disponibles ?', 10, '2017-04-23 19:42:16');

-- --------------------------------------------------------

--
-- Structure de la table `ref_contenu_produit`
--

CREATE TABLE `ref_contenu_produit` (
  `id` int(11) NOT NULL,
  `id_produit` int(11) NOT NULL,
  `id_contenu` int(11) NOT NULL,
  `pin` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `ref_contenu_produit`
--

INSERT INTO `ref_contenu_produit` (`id`, `id_produit`, `id_contenu`, `pin`) VALUES
(20, 17, 118, 0),
(21, 17, 119, 0),
(22, 17, 120, 0),
(23, 10, 121, 0),
(24, 12, 121, 0),
(25, 13, 123, 0),
(26, 14, 123, 0),
(27, 15, 125, 0),
(28, 16, 119, 0),
(29, 16, 120, 0),
(31, 10, 129, 0),
(32, 12, 129, 0),
(34, 13, 132, 0),
(35, 14, 132, 0),
(36, 14, 134, 0),
(37, 13, 134, 0),
(39, 15, 137, 0),
(42, 16, 140, 0),
(44, 17, 142, 0),
(45, 17, 143, 0),
(52, 17, 150, 0);

-- --------------------------------------------------------

--
-- Structure de la table `reponse`
--

CREATE TABLE `reponse` (
  `id` int(11) NOT NULL,
  `idQuestion` int(11) NOT NULL,
  `text` text NOT NULL,
  `idVendeur` int(11) NOT NULL,
  `solution` tinyint(1) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Contenu de la table `reponse`
--

INSERT INTO `reponse` (`id`, `idQuestion`, `text`, `idVendeur`, `solution`, `date`) VALUES
(37, 25, 'Laver à 30°C sur un programme délicat', 10, 1, '2017-04-23 19:35:02'),
(38, 26, 'Normalement, oui !', 10, 0, '2017-04-23 19:41:05'),
(39, 26, 'Par contre, les coloris pourraient varier...', 8, 0, '2017-04-23 19:41:46'),
(40, 27, 'Noir et rouge, le bleu est en rupture actuellement', 8, 0, '2017-04-23 19:43:35');

-- --------------------------------------------------------

--
-- Structure de la table `vendeur`
--

CREATE TABLE `vendeur` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `secteur` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `vendeur`
--

INSERT INTO `vendeur` (`id`, `nom`, `secteur`, `image`, `username`, `password`) VALUES
(8, 'Thomas Soto', '', 'd052d71beedf661339f3b731551790b8.jpg', 'thomas', 'test'),
(9, 'Florence Stone', '', 'f1162321809cae9280365540bbf27726.jpg', 'florence', 'test'),
(10, 'Béatrice Martin', '', 'c2c4edcd3976b7e6b3b741a612c25a0d.jpg', 'beatrice', 'test');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `contenu_social`
--
ALTER TABLE `contenu_social`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `r_id` (`r_id`),
  ADD UNIQUE KEY `r_id_2` (`r_id`);

--
-- Index pour la table `produit`
--
ALTER TABLE `produit`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `ref_contenu_produit`
--
ALTER TABLE `ref_contenu_produit`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_produit` (`id_produit`,`id_contenu`);

--
-- Index pour la table `reponse`
--
ALTER TABLE `reponse`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `vendeur`
--
ALTER TABLE `vendeur`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `contenu_social`
--
ALTER TABLE `contenu_social`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;
--
-- AUTO_INCREMENT pour la table `produit`
--
ALTER TABLE `produit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT pour la table `question`
--
ALTER TABLE `question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT pour la table `ref_contenu_produit`
--
ALTER TABLE `ref_contenu_produit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT pour la table `reponse`
--
ALTER TABLE `reponse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT pour la table `vendeur`
--
ALTER TABLE `vendeur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
