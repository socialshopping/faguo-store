var mysql      = require('mysql');
var config     = require('./config/default');

var connection = mysql.createConnection({
  host     : config.mysql_host,
  user     : config.mysql_user,
  password : config.mysql_password,
  database : config.mysql_database
});

module.exports = {

    listQuestions: function() {
        return new Promise((resolve, reject) => {
            connection.query(`
                SELECT 
                    question.id AS qid, question.idProduit AS qidproduit, question.text AS qtext, question.date AS qdate,
                    produit.id AS pid, produit.nom AS pnom, produit.type AS ptype, produit.description AS pdesc, produit.image AS pimage,
                    qvendeur.nom AS qvnom, qvendeur.id AS qvid, qvendeur.image AS qvimage,
                    count(reponse.id) AS rcount
                FROM question
                LEFT JOIN reponse ON reponse.idQuestion = question.id
                LEFT JOIN vendeur AS qvendeur ON question.idVEndeur = qvendeur.id
                LEFT JOIN vendeur AS rvendeur ON reponse.idVEndeur = rvendeur.id
                LEFT JOIN produit ON produit.id = question.idProduit
                GROUP BY qid
                ORDER BY qdate DESC
            `, (err, result, fields) => {
                if (err) return reject(err);
                let questions = [];
                let curr;
                result.forEach(row => questions.push({
                    id: row.qid,
                    idProduit: row.qidproduit,
                    text: row.qtext,
                    vendeur: {
                        id: row.qvid,
                        nom: row.qvnom,
                        image: row.qvimage
                    },
                    produit: {
                        id: row.pid,
                        nom: row.pnom,
                        description: row.pdesc,
                        type: row.ptype,
                        image: row.pimage
                    },
                    date: row.qdate,
                    rCount: row.rcount
                }));
                resolve(questions);
            });
        });
    },

    listProductQuestions: function(idProduit) {
        return new Promise((resolve, reject) => {
            connection.query(`
                SELECT 
                    question.id AS qid, question.idProduit AS qidproduit, question.text AS qtext, question.date AS qdate,
                    qvendeur.nom AS qvnom, qvendeur.id AS qvid, qvendeur.image AS qvimage,
                    count(reponse.id) AS rcount
                FROM question
                LEFT JOIN reponse ON reponse.idQuestion = question.id
                LEFT JOIN vendeur AS qvendeur ON question.idVEndeur = qvendeur.id
                LEFT JOIN vendeur AS rvendeur ON reponse.idVEndeur = rvendeur.id
                WHERE question.idProduit = ?
                GROUP BY qid
                ORDER BY qdate DESC
            `, [idProduit], (err, result, fields) => {
                if (err) return reject(err);
                let questions = [];
                let curr;
                result.forEach(row => questions.push({
                    id: row.qid,
                    idProduit: row.qidproduit,
                    text: row.qtext,
                    vendeur: {
                        id: row.qvid,
                        nom: row.qvnom,
                        image: row.qvimage
                    },
                    date: row.qdate,
                    rCount: row.rcount
                }));
                resolve(questions);
            });
        });
    },

    getQuestion: function(id) {
        return new Promise((resolve, reject) => {
            connection.query(`
                SELECT 
                    question.id AS qid, question.idProduit AS qidproduit, question.text AS qtext, question.date AS qdate,
                    produit.id AS pid, produit.nom AS pnom, produit.type AS ptype, produit.description AS pdesc, produit.image AS pimage,
                    qvendeur.nom AS qvnom, qvendeur.id AS qvid, qvendeur.image AS qvimage,
                    rvendeur.nom AS rvnom, rvendeur.id AS rvid, rvendeur.image AS rvimage,
                    reponse.id AS rid, reponse.text AS rtext, reponse.solution AS rsol, reponse.date as rdate
                FROM question
                LEFT JOIN reponse ON reponse.idQuestion = question.id
                LEFT JOIN vendeur AS qvendeur ON question.idVEndeur = qvendeur.id
                LEFT JOIN vendeur AS rvendeur ON reponse.idVEndeur = rvendeur.id
                LEFT JOIN produit ON produit.id = question.idProduit
                WHERE question.id = ?
                ORDER BY rdate
            `, [id], (err, result, fields) => {
                if (err) return reject(err);
                if (result.length < 1) return reject(new Error('Error: no record found !'));
                let row = result[0];
                let question = {
                    id: row.qid,
                    idProduit: row.qidproduit,
                    text: row.qtext,
                    vendeur: {
                        id: row.qvid,
                        nom: row.qvnom,
                        image: row.qvimage
                    },
                    produit: {
                        id: row.pid,
                        nom: row.pnom,
                        description: row.pdesc,
                        type: row.ptype,
                        image: row.pimage
                    },
                    date: row.qdate,
                    reponses: []
                };
                result.forEach(row => {
                    if (!row.rid) return;
                    question.reponses.push({
                        id: row.rid,
                        text: row.rtext,
                        solution: row.rsol == 1,
                        date: row.rdate,
                        vendeur: {
                            id: row.rvid,
                            nom: row.rvnom,
                            image: row.rvimage
                        }
                    });
                });
                resolve(question);
            });
        });
    },

    addQuestion: function(question) {
        return new Promise((resolve, reject) => {
            connection.query(`
                INSERT INTO question (idProduit, text, idVendeur, date)
                VALUES (?, ?, ?, NOW())
            `, [
                question.produit ? question.produit.id || question.idProduit : question.idProduit, 
                question.text,
                question.vendeur ? question.vendeur.id || question.idVendeur : question.idVendeur
            ], (err, result) => {
                if (err) return reject(err);
                resolve(result.insertId);
            });
        }).then(id => this.getQuestion(id));
    },

    addReponse: function(idQuestion, reponse) {
        return new Promise((resolve, reject) => {
            connection.query(`
                INSERT INTO reponse (idQuestion, text, idVendeur, date)
                VALUES (?, ?, ?, NOW())
            `, [
                idQuestion,
                reponse.text,
                reponse.vendeur ? reponse.vendeur.id || reponse.idVendeur : reponse.idVendeur
            ], (err, result) => {
                if (err) return reject(err);
                resolve();
            });
        }).then(() => this.getQuestion(idQuestion));
    },

    listProducts: function () {
        return new Promise((resolve, reject) => {
            connection.query('SELECT id, nom, type, description, image FROM produit', (err, result, fields) => {
                if (err) return reject(err);
                else resolve(result);
            });
        });
    },

    getProduct: function (id) {
        return new Promise((resolve, reject) => {
            connection.query('SELECT id, nom, type, description, image FROM produit WHERE id = ?', [id], (err, result, fields) => {
                if (err) return reject(err);
                else if (result.length < 0) reject('NOT FOUND');
                else resolve(result[0]);
            });
        });
    },

    searchProductBarcode: function(code) {
        return new Promise((resolve, reject) => {
            connection.query('SELECT id, nom, type, description, image FROM produit WHERE barcode = ?', [code], (err, result, fields) => {
                if (err) return reject(err);
                else if (result.length < 0) reject('NOT FOUND');
                else resolve(result[0]);
            });
        });
    },

    pinContenuRef: function(id) {
        return new Promise((resolve, reject) => {
            connection.query(`
                UPDATE ref_contenu_produit
                SET pin = 1-pin
                WHERE id = ?
            `,
            [id], (err, result, fields) => resolve(result));
        });
    },

    updateProduct: function (id, produit) {
          return new Promise((resolve,reject) => {
            connection.query(`
              UPDATE produit
              SET (nom, type, description, image) = (?, ?, ?, ?)
              WHERE id = ?
            `,
            [produit.nom, produit.type, produit.description, produit.image, id], (err, result, fields) => resolve(result));
          });
    },

    deleteProduct: function (id) {
      return new Promise((resolve,reject) => {
        connection.query(`
          DELETE FROM produit
          WHERE id = ?
          `,
          [id], (err, result, fields) => resolve(result));
        });
      },

    addProduct: function (produit) {
      return new Promise((resolve,reject) => {
        connection.query(`
          INSERT INTO produit(nom, type, description, image) VALUES (?, ?, ?, ?)`, [produit.nom, produit.type, produit.description, produit.image],
          (err, result, fields) => resolve(Object.assign(produit, {id: result.insertId})));
      }
    );
  },


    addSocialContent: function(content, idProduit) {
        return Promise.all(
            content.map(post => {
                return new Promise((resolve, reject) => {
                    connection.query(`
                        INSERT INTO contenu_social(type, nom_auteur, r_id, description, media, ref_produit, date_publication)
                        VALUES (?, ?, ?, ?, ?, ?, ?)
                        ON DUPLICATE KEY UPDATE id = LAST_INSERT_ID(id), r_id = r_id
                    `, [
                        post.type, post.nom_auteur, post.id, post.description, post.media, idProduit, post.date_publication,
                    ], (err, result, fields) => err ? reject(err) : resolve(result));
                }).then(result => {
                    return new Promise((resolve, reject) => {
                        connection.query(`
                            INSERT INTO ref_contenu_produit (id_produit, id_contenu)
                            VALUES (?, ?)
                            ON DUPLICATE KEY UPDATE id = LAST_INSERT_ID(id)
                        `, [idProduit, result.insertId], (err, result, fields) => resolve(result));
                    });
                }).catch(err => console.error(post,err));
            })
        ).then(posts => posts.filter(x => x));
    },

    listSocialContent: function(idProduit) {
        return new Promise((resolve, reject) => {
            connection.query(`
                SELECT contenu_social.id, ref_contenu_produit.id as idRef, contenu_social.type, contenu_social.nom_auteur, contenu_social.r_id, contenu_social.description, contenu_social.media, contenu_social.date_publication , ref_contenu_produit.pin
                FROM ref_contenu_produit
                INNER JOIN contenu_social ON contenu_social.id = ref_contenu_produit.id_contenu
                WHERE ref_contenu_produit.id_produit = ?
                ORDER BY pin DESC, date_publication DESC
            `, [idProduit], (err, result, fields) => {
                if (err) return reject(err);
                else resolve(result);
            });
        });
    },

    listSocialBrandContent: function() {
        return new Promise((resolve, reject) => {
            connection.query(`
                SELECT contenu_social.id, contenu_social.type, contenu_social.nom_auteur, contenu_social.r_id, contenu_social.description, contenu_social.media, contenu_social.date_publication
                FROM ref_contenu_produit
                INNER JOIN contenu_social ON contenu_social.id = ref_contenu_produit.id_contenu
                WHERE ref_contenu_produit.id_produit IS NULL
                ORDER BY date_publication DESC
            `, (err, result, fields) => {
                if (err) return reject(err);
                else resolve(result);
            });
        });
    },

    findVendeurByIdentifiant: function (username) {
        return new Promise((resolve, reject) => {
            connection.query('SELECT id, nom, secteur, image, username, password FROM vendeur WHERE username = ?', [username], (err, result, fields) => {
                if (err) return reject(err);
                else resolve(result);
            });
        });
    },

    listVendeurs: function () {
        return new Promise((resolve, reject) => {
            connection.query('SELECT id, nom, secteur, image, username, password FROM vendeur', (err, result, fields) => {
                if (err) return reject(err);
                else resolve(result);
            });
        });
    },

    getVendeur: function (id) {
        return new Promise((resolve, reject) => {
            connection.query('SELECT id, nom, secteur, image, username, password FROM vendeur WHERE id = ?', [id], (err, result, fields) => {
                if (err) return reject(err);
                else if (result.length < 0) reject('NOT FOUND');
                else resolve(result[0]);
            });
        });
    },

    getVendeurList: function(idList) {
        return new Promise((resolve, reject) => {
            connection.query('SELECT id, nom, secteur, image, username, password FROM vendeur WHERE id IN (?)', idList.join(','), (err, result, fields) => {
                if (err) return reject(err);
                else resolve(result);
            });
        });
    },


    addVendeur: function (vendeur) {
      return new Promise((resolve,reject) => {
        connection.query(`
          INSERT INTO vendeur(nom, secteur, image, username, password) VALUES (?, ?, ?, ?, ? )`, [vendeur.nom, vendeur.secteur, vendeur.image, vendeur.username, vendeur.password],
          (err, result, fields) => resolve(Object.assign(vendeur, {id: result.insertId})));
      }
    );
  },

    deleteVendeur: function (id) {
        return new Promise((resolve,reject) => {
            connection.query(`
                DELETE FROM vendeur
                WHERE id = ?
                `,
                [id],
                (err, result, fields) => resolve(result));
            }
        );
    },

    updateVendeur: function (id, vendeur) {
        return new Promise((resolve,reject) => {
            connection.query(`
                UPDATE vendeur
                SET nom = ?, secteur = ?, username = ?
                WHERE id = ?
            `,
            [vendeur.nom, vendeur.secteur, vendeur.username, id], (err, result, fields) => resolve(result));
        });
    }

};


connection.connect(err => {
    if (err) {
        console.error(err);
    } else {
        console.log('BASE DE DONNEES CONNECTEE');
    }
});
