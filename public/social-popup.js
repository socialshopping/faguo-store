var openPopup;

$(document).ready(function() {
    var content = $('.content');
    var socialPopup = $('.social-popup');
    var productContent = $('.product-content');
    var pageContent = $('.page-content');
    var gears = $('.social-popup > .gears');
    
    var displayedPosts = {
        product: [],
        brand: []
    };
    
    $('#search-post').on('keyup', function() {
        var val = $(this).val().toLowerCase();
        productContent.empty();
        pageContent.empty();
        displayedPosts.product.forEach(function(p) {
            if (p.description.toLowerCase().indexOf(val) > -1) productContent.append(p.block);
        });
        displayedPosts.brand.forEach(function(p) {
            if (p.description.toLowerCase().indexOf(val) > -1) pageContent.append(p.block);
        });
    });

    openPopup = function (productId) {
        $('body').css('overflow', 'hidden');
        productContent.empty();
        pageContent.empty();
        gears.fadeIn();
        
        $('.social-popup').fadeIn();
        
        console.log(productId);
        
        $.ajax({
            url: '/social/api/'+productId,
            type: 'GET',
            datatype: 'application/json',
            success: function(response) {
                displayedPosts.product = [];
                response.forEach(function (element, index, array){
                    element.mediaTag = element.media ? '<img src="'+element.media+'" /></div>' : '';
                    element.block = $(Mustache.render($('#post-template').html(), element));
                    productContent.append(element.block);
                    displayedPosts.product.push(element);
                    element.block.fadeIn();
                });
                gears.fadeOut();
            },
        });
        
        $.ajax({
            url: '/social/api',
            type: 'GET',
            datatype: 'application/json',
            success: function(response) {
                displayedPosts.brand = [];
                response.forEach(function (element, index, array){
                    element.mediaTag = element.media ? '<img src="'+element.media+'" /></div>' : '';
                    element.block = $(Mustache.render($('#post-template').html(), element));
                    pageContent.append(element.block);
                    displayedPosts.brand.push(element);
                    element.block.fadeIn();
                });
                gears.fadeOut();
            },
        });
    }

    $('.social-button').click(function(e) {
        
        var productId = $(this).data('productId');
        openPopup(productId);
        
    });
    
    socialPopup.click(function(e) {
        socialPopup.fadeOut();
        $('body').css('overflow', 'initial');
    })
    
    $(socialPopup).children().click(function(e) {
        e.stopPropagation();
    })
});
