function ChatBox(me, dest, socket) {
    var th = this;
    this.server = socket;
    this.block = $(Mustache.render($('#chatbox-template').html(), dest));
    this.me = me;
    this.dest = dest;
    $('.chat-container').append(this.block);

    this.block.find('.header').click(this.toggle.bind(this));
    this.msgList = this.block.find('.body');
    this.lastUser = null;

    this.textarea = this.block.find('textarea');
    this.sendBtn = this.block.find('button');

    this.server.on('text.receive', this.onNewMessage.bind(this));

    this.sendBtn.click(this.sendMessage.bind(this));

    this.textarea.keydown(function(e) {
        if (e.keyCode !== 13 || e.shiftKey) return;
        th.sendMessage();
        e.preventDefault();
    });
}

ChatBox.prototype.onNewMessage = function(message) {
    if (message.from == this.dest.id || message.to == this.dest.id) {
        this.addMessage(message);
    }
}

ChatBox.prototype.sendMessage = function() {
    var text = this.textarea.val();
    this.server.emit('text.send', this.dest.id, text);
    //this.addMessage({ from: this.me, text: text, date: new Date() });
    this.textarea.val('');
};

ChatBox.prototype.open = function() {
    this.block.find('.body').show();
    this.block.find('a.fa-times').show();
    this.block.find('a.fa-external-link-square').hide();
};

ChatBox.prototype.toggle = function() {
    this.block.find('.body').toggle();
    this.block.find('a').toggle();
};

ChatBox.prototype.addMessage = function(message) {
    message.own = this.me === message.from;
    if (message.from == this.dest.id) message.image = this.dest.image;
    if (this.lastUser === message.from) {
        var lastMsg = this.msgList.children().last();
        var line = $('<div class="line">'+message.text+'</div>');
        lastMsg.find('.infos').before(line);
        lastMsg.find('.infos').html('Envoyé à '+message.date.toString());
    } else {
        this.msgList.append($(Mustache.render($('#chatbox-message').html(), message)));
        this.msgList.animate({ scrollTop: this.msgList.height() });
    }
    this.lastUser = message.from;
};


var ChatInterface = function ChatInterface(id, element) {
    this.element = element;
    this.id = id;
    this.userlist = element.find('ul');

    var own = this;

    this.server = io.connect();

    own.server.emit('signin', id)

    this.server.on('users.online', function(users) {
        own.userlist.empty();
        own.online_users = {};
        own.chatBoxes = {};
        users.forEach(own.onUserConnect.bind(own));
    });

    this.server.on('backup.conv', function(id, msgList) {
        var vendeur = own.online_users[id];
        if (!vendeur) return;
        if (own.chatBoxes[vendeur.id]) return;
        own.chatBoxes[vendeur.id] = new ChatBox(own.id, vendeur, own.server);
        msgList.forEach(function(message) {
            own.chatBoxes[vendeur.id].addMessage(message);
        });
    });

    this.server.on('text.receive', function(message) {
        var vendeur;
        if (message.from == own.id) vendeur = own.online_users[message.to];
        else {
            vendeur = own.online_users[message.from];
            own.sendNotification(vendeur.nom+' : '+message.text, function() { window.focus(); this.close() });
        }
        if (own.chatBoxes[vendeur.id]) own.chatBoxes[vendeur.id].open();
        else {
            own.chatBoxes[vendeur.id] = new ChatBox(own.id, vendeur, own.server);
            own.chatBoxes[vendeur.id].onNewMessage(message);
        }
    });

    this.server.on('question.new', function(author, text, idQuestion) {
        own.sendNotification(
            'Nouvelle question de '+author+' : '+text,
            function() { window.focus(); this.close(); window.location.href = "/vendeur/questions?question_id="+idQuestion }
        );
    });

    $('.video-overlay button').click(this.cleanUpVideoCall.bind(this));

    this.server.on('video.invite', this.onVideoInvite.bind(this));
    this.server.on('video.confirm', this.onVideoConfirm.bind(this));

    this.server.on('video.decline', own.cleanUpVideoCall.bind(this));

    own.server.on('users.connect', this.onUserConnect.bind(this));
    own.server.on('users.disconnect', this.onUserDisconnect.bind(this));
};

ChatInterface.prototype.sendNotification = function(content, callback) {
    try {
        var notification;
        if (Notification.permission === "granted") {
            notification = new Notification(content);
        } else if (Notification.permission !== 'denied') {
            Notification.requestPermission(function (permission) {
                if(!('permission' in Notification)) {
                    Notification.permission = permission;
                }

                if (permission === "granted") {
                    notification = new Notification(content);
                }
            });
        }
        if (notification) notification.onclick = callback;
    } catch (e) {
        console.warn('Notification not supported !');
    }
};

ChatInterface.prototype.onContactPressed = function(e) {
    var vendeur = this.online_users[$(e.target).data('id')];
    if (this.chatBoxes[vendeur.id]) own.chatBoxes[vendeur.id].open();
    else this.chatBoxes[vendeur.id] = new ChatBox(this.id, vendeur, this.server);
};

ChatInterface.prototype.onCallInitiate = function(e) {
    if (this.peer) return;
    var own = this;
    var vendeur = this.online_users[$(e.target).data('id')];

    navigator.getUserMedia({ video: true, audio: true }, function(stream) {
        own.peer = new SimplePeer({
            initiator: true,
            stream: stream
        });
        own.stream = stream;

        own.peer.on('stream', function(remoteStream) {
            $('.video-overlay').css('display', 'flex');
            var video = $('video.chat-video');
            video[0].src = window.URL.createObjectURL(remoteStream);
            video[0].play();

            var localVideo = $('video.self-video');
            localVideo[0].src = window.URL.createObjectURL(stream);
            localVideo[0].play();
        });
        own.peer.on('close', function() {
            own.cleanUpVideoCall();
        });
        own.peer.on('signal', function(sdp) {
            own.server.emit('video.invite', vendeur.id, sdp);
        });
    }, function () {})
};

ChatInterface.prototype.cleanUpVideoCall = function() {
    this.connecting = false;
    if (this.stream) this.stream.getTracks().forEach(function(track) {
        track.stop();
    });
    $('.video-overlay').hide();
    $('.modal-overlay').hide();
    $('.modal button').off();
    if (!this.peer) return;
    this.peer.destroy();
    delete this.peer;
}

ChatInterface.prototype.onVideoInvite = function(id, sdp, socketId) {
    var own = this;
    if (own.connecting && this.peer) return this.peer.signal(sdp);
    else if (own.connecting) return;
    own.connecting = true;
    if (own.peer) {
        own.server.emit('video.decline', id);
        return;
    } else {
        var vendeur = this.online_users[id];
        this.sendNotification(vendeur.nom+' souhaite démarrer une conversation vidéo.', function() { window.focus(); this.close() });
        $('.modal-overlay').css('display', 'flex');
        $('.modal .vendeur').html(vendeur.nom);
        $('.modal button').one('click', function(e) {
            $('.modal-overlay').hide();
            if ($(e.target).data('type') === 'decline') {
                own.connecting = false;
                own.server.emit('video.decline', id);
            } else {
                navigator.getUserMedia({ video: true, audio: true }, function(stream) {
                    own.peer = new SimplePeer({
                        initiator: false,
                        stream: stream
                    });
                    own.stream = stream;
                    own.peer.on('stream', function(remoteStream) {
                        $('.video-overlay').css('display', 'flex');
                        var video = $('video.chat-video');
                        video[0].src = window.URL.createObjectURL(remoteStream);
                        video[0].play();

                        var localVideo = $('video.self-video');
                        localVideo[0].src = window.URL.createObjectURL(stream);
                        localVideo[0].play();
                    });

                    own.peer._socketId = socketId;
                    own.peer._userId = id;
                    own.peer.signal(sdp);
                    own.peer.on('close', function() {
                        own.cleanUpVideoCall();
                    });
                    own.peer.on('signal', function(data) {
                        own.server.emit('video.confirm', vendeur.id, data, socketId);
                    });

                    $()
                }, function () {})
            }
        });
    }
};

ChatInterface.prototype.onVideoConfirm = function(sdp, socketId) {
    if (!this.peer) return;
    this.connecting = true;
    this.peer._socketId = socketId;
    this.peer.signal(sdp);
};

ChatInterface.prototype.onUserConnect = function(vendeur) {
    if (this.online_users[vendeur.id]) return;
    vendeur.widget = $(Mustache.render($('#userline-template').html(), vendeur));
    vendeur.widget.find('button.text-open').click(this.onContactPressed.bind(this));
    vendeur.widget.find('button.video-call').click(this.onCallInitiate.bind(this));
    this.online_users[vendeur.id] = vendeur;
    this.userlist.append(vendeur.widget);
};

ChatInterface.prototype.onUserDisconnect = function(id) {
    var vendeur = this.online_users[id];
    if (!vendeur) return;
    vendeur.widget.remove();
    delete this.online_users[id];
};

ChatInterface.prototype.onNewMessage = function(id, invite) {
    
}