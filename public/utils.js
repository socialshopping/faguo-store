var template = function (name) {
    console.log(name);
    var tpl = $('script[type="text/mustache-template"]').filter('#'+name).html();
    return function(data) {
        return Mustache.render(
            tpl,
            data
        );
    }
}

var Box = function(element, itemTemplate, detailsTemplate, itemLoad) {
    var self = this;
    Object.assign(self, {
        element: element,
        listPane: element.find('.list-pane'),
        detailsPane: element.find('.details-pane'),
        noSelection: element.find('.no-selection'),

        itemTemplate: itemTemplate,
        detailsTemplate: detailsTemplate,
        itemLoad: itemLoad,

        selected: null
    });
}

Box.prototype.close = function() {
    var self = this;
    if (self.selected) {
        self.detailsPane.hide();
        self.listPane.show();
        if (!tablet()) self.noSelection.show();
    }
}

Box.prototype.open = function(id) {
    var self = this;
    self.noSelection.hide();
    self.selected = id;

    if(tablet()) self.listPane.hide();
    
    self.itemLoad(id, function(data) {
        self.detailsPane.html(self.detailsTemplate(data));
        self.detailsPane.find('.back-to-list').click(self.close.bind(self));
        self.detailsPane.show();
    });
}

Box.prototype.addItems = function(items) {
    var self = this;
    items.forEach(function(item) {
        var elt = $(self.itemTemplate(item));
        self.listPane.find('ul').append(elt);
        elt.find('a').click(function(e) {
            self.open(item.id);
        });
    });
}