var fetch   = require('universal-fetch');
var config  = require('../config/default');
var HttpsProxyAgent = require('https-proxy-agent');

var FAGUO_PAGE_ID = 88944535935;

var access_token;

module.exports = {
    /* Convertir un token utilisateur vers un token à durée de vie étendue
     *  - token: le token utilisateur
     */
    convertToken: token => fetch(
        `https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=${config.facebook_appId}&client_secret=${config.facebook_secret}&fb_exchange_token=${token}`
        /*, { agent: new HttpsProxyAgent('http://proxy.ec-lille.fr:3128') }*/
    ).then(r => r.text()).then(body => {
        access_token = JSON.parse(body).access_token;
        if (access_token) {
            return access_token;
        } else throw('Erreur !')
    }),

    getToken: () => access_token,

    /* Rechercher du contenu sur l'API Facebook
     *  - nbPages: nombres de pages sur lesquelles itérer
     *  - produit: [FACULTATIF] produit pour lequel les résultats doivent être retournés
     *  - url: [FACULTATIF] url à laquelle rechercher le contenu (par défaut, la recherche se lance sur la page facebook de FAGUO)
     */
    searchContent: (nbPages, produit, url) => new Promise((resolve, reject) => {
        if (nbPages < 1) return resolve([]);
        fetch(url || `https://graph.facebook.com/${FAGUO_PAGE_ID}/feed?access_token=${access_token}&fields=message,full_picture,story,from,comments,created_time`
            /*, { agent: new HttpsProxyAgent('http://proxy.ec-lille.fr:3128') }*/).then(r => r.json()).then(body => {
            if (!body.data) reject(body);
            var filtered_posts = produit ?
                body.data.filter(post => {
                    if (!post.message) return false;
                    var keywords = produit.nom.split(' ');
                    return post.message.toLowerCase().indexOf(keywords[0].toLowerCase()) > -1;
                }) :
                body.data;
            filtered_posts = filtered_posts.map(post => ({
                type: 'facebook',
                nom_auteur: post.from.name,
                date_publication: new Date(post.created_time),
                description: post.message,
                media: post.full_picture,
                id: post.id
            }));
            return module.exports.searchContent(
                nbPages-1,
                produit,
                body.paging.next
            ).then(array => resolve(array.concat(filtered_posts)));
        });
    })
}
