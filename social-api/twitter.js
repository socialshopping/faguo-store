var fetch   = require('universal-fetch');
var config  = require('../config/default');
var HttpsProxyAgent = require('https-proxy-agent');

module.exports = {
    /* Rechercher du contenu sur l'API Facebook
     *  - produit: [FACULTATIF] produit pour lequel les résultats doivent être retournés
     */
    searchContent: (produit) => fetch(
        `https://api.twitter.com/1.1/search/tweets.json?q=${produit ? 'faguo '+produit.nom.split(' ')[0] : 'faguo'}&result_type=recent&count=20`, {
        headers: {
            Authorization: `Bearer ${config.twitter_bearer}`
        }
        /*, agent: new HttpsProxyAgent('http://proxy.ec-lille.fr:3128')*/
    }).then(r => r.json()).then(data => data.statuses).then(posts => posts.map(post => ({
        type: 'twitter',
        nom_auteur: post.user.name,
        date_publication: new Date(post.created_at),
        description: post.text,
        media: post.entities.media && post.entities.media.length > 0 ? post.entities.media[0].media_url : undefined,
        id: post.id
    }))),

    /* Rechercher du contenu sur l'API Twitter
     *  - id : identifiant de la publication à récupérer
     */
    getTweet: id => fetch(
        `https://api.twitter.com/1.1/statuses/show.json?id=${id}`, {
        headers: {
            Authorization: `Bearer ${config.twitter_bearer}`
        }
        /*, agent: new HttpsProxyAgent('http://proxy.ec-lille.fr:3128')*/
    }).then(r => r.json()).then(tweet => { console.log(tweet.entities.media); return ({
        type: 'twitter',
        nom_auteur: tweet.user.name,
        date_publication: new Date(tweet.created_at),
        description: tweet.text,
        media: tweet.entities.media && tweet.entities.media.length > 0 ? tweet.entities.media[0].media_url : undefined,
        id: tweet.id
    })}),
}
