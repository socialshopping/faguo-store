'use strict'

const crypto = require("crypto");
const database = require('./database');

module.exports = function chatModule(io) {
    var conversations = {};
    var online_users = {};

    io.on('connection', function (socket) {
        socket._socketId = crypto.randomBytes(16).toString("hex");
        socket.on('signin', (id) => {
            if (online_users[id] && online_users[id].filter(s => s._socketId === socket._socketId).length > 0) return;

            console.log('signin', id);

            socket._userId = id;
            if (!conversations[id]) conversations[id] = {};
            
            // Récupération des données des utilisateurs connectés
            database.getVendeurList(
                Object.keys(online_users).filter(i => i !== id)
            ).then(vendeurs => vendeurs.map(v => {
                delete v.password;
                return v;
            })).then(vendeurs => {
                socket.emit('users.online', vendeurs);
            });

            database.getVendeur(id).then(vendeur => {
                // Association de la socket à l'utilisateur connect
                if (online_users[id]) {
                    online_users[id].push(socket);
                } else {
                    online_users[id] = [socket];
                    socket.broadcast.emit('users.connect', Object.assign(vendeur, { sdp: [socket._sdp] }));
                }

                Object.keys(conversations[id]).forEach(destId => {
                    socket.emit('backup.conv', destId, conversations[id][destId]);
                });
            });

            

            socket.on('disconnect', () => {
                console.log('disconnect', socket._userId);
                if (!online_users[socket._userId]) return;
                online_users[socket._userId] = online_users[socket._userId].filter(s => s._socketId !== socket._socketId);
                if (online_users[socket._userId].length < 1) {
                    delete online_users[socket._userId];
                    socket.broadcast.emit('users.disconnect', socket._userId);
                }
            });

            socket.on('text.send', (id, text) => {
                var message = { from: socket._userId, to: id, text, date: new Date() };
                if(!conversations[id][socket._userId]) {
                    var arr = [];
                    conversations[id][socket._userId] = arr;
                    conversations[socket._userId][id] = arr;
                }
                conversations[id][socket._userId].push(message);
                online_users[id].forEach(s => {
                    s.emit('text.receive', message);
                });
                online_users[socket._userId].forEach(s => {
                    s.emit('text.receive', message);
                });
            });

            socket.on('video.invite', (id, sdp) => {
                var dest = online_users[id];
                console.log('video.invite');
                dest.forEach(s => s.emit('video.invite', socket._userId, sdp, socket._socketId));
            });

            socket.on('video.confirm', (id, sdp, socketId) => {
                var dest = online_users[id].filter(s => s._socketId === socketId)[0];
                if (!dest) return;
                console.log('video.confirm');
                dest.emit('video.confirm', sdp);
            });

            socket.on('video.decline', (id) => {
                var dest = online_users[id];
                console.log('video.decline');
                dest.forEach(s => s.emit('video.decline', socket._userId));
            });
        });
        socket.on('message', function(userId, message) {
            io.emit('message', 10, { content: message, date: new Date().toString() })
        });
    });


    return {
        getonline_users: () => Object.keys(online_users)
    }
}