var express = require('express');
var app     = express();
var path    = require('path');
var fetch   = require('universal-fetch');
var bodyParser = require('body-parser')
var crypto  = require('crypto');

var config  = require('./config/default');

var FB      = require('./social-api/facebook');
var Twitter = require('./social-api/twitter');
var passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy;


app.use(express.static('./public'));
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(require('express-session')({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true
}));
app.use(require('connect-flash')());
app.use(passport.initialize());
app.use(passport.session());
app.set('view engine', 'ejs');


var server  = require('http').Server(app);
var io      = require('socket.io')(server);

app.set('io', io);

var chat    = require('./chat')(io);

server.listen(2000, () => console.log('PORT 2000'));

var database = require('./database');

app.use('/', require('./routes'));

app.locals.fbAppID = config.facebook_appId;

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

passport.use(new LocalStrategy(
    function(identifiant, password, done) {
        database.findVendeurByIdentifiant(identifiant).then(user => {
            if (!user || user.length < 1) {
                return done(null, false);
            } else if (user[0].password !== password) {
                return done(null, false);
            } else return done(null, user[0]);
        });
    }
));

app.post('/vendeur/login',
  passport.authenticate('local', { successRedirect: '/vendeur',
                                   failureRedirect: '/vendeur/login',
                                   failureFlash: true,
                                   successFlash: 'Bienvenue !',
                                   failureFlash: 'Vos informations de connexion ne permettent pas de vous authentifier' }));


app.get('/social/facebook/access_token', (req, res) => {
    if (req.query.token) FB.convertToken(req.query.token).then(token => res.send(token)).catch(e => res.send(e));
    else {
        var token = FB.getToken();
        res.send(token ? token : 'NOT_CONNECTED');
    }
});


// Import du contenu Facebook pour un produit donné
app.get('/social/facebook/import/:idProduit', (req, res) => {
    database.getProduct(req.params.idProduit)
        .then(produit => FB.searchContent(20, produit))
        .then(posts => database.addSocialContent(posts, req.params.idProduit))
        .then(posts => res.send(posts))
        .catch(e => res.status(500).send(e));
});

// Import du contenu Facebook pour la marque
app.get('/social/facebook/import', (req, res) => {
    FB.searchContent(1)
        .then(posts => database.addSocialContent(posts))
        .then(posts => res.json(posts))
        .catch(e => res.status(500).send(e));
});

// Import du contenu Twitter pour un produit donné
app.get('/social/twitter/import/:idProduit', (req, res) => {
    database.getProduct(req.params.idProduit)
        .then(produit => Twitter.searchContent(produit))
        .then(posts => database.addSocialContent(posts, req.params.idProduit))
        .then(posts => res.send(posts))
        .catch(e => res.status(500).send(e));
});

app.get('/social/import/:idProduit', (req, res) => {
    database.getProduct(req.params.idProduit)
        .then(produit => Promise.all([
            Twitter.searchContent(produit),
            FB.searchContent(20, produit)
        ])).then(([tposts, fposts]) => database.addSocialContent(tposts.concat(fposts), req.params.idProduit))
        .then(posts => res.send(posts))
        .catch(e => res.status(500).send(e.stack));
});

app.get('/social/api/:idProduit', (req, res) => database.listSocialContent(req.params.idProduit).then(data => res.json(data)).catch(e => res.status(500).send(e)));

app.get('/social/api', (req, res) => database.listSocialBrandContent().then(data => res.json(data)).catch(e => res.status(500).send(e)));

app.post('/social/suggest/:idProduit', (req, res) => {
    if (/(https?:\/\/)?(www\.)?twitter\.com\/([a-z0-9_]+)\/status\/([a-z0-9]+)/i.test(req.body.url)) {
        var user = RegExp.$3, r_id = RegExp.$4;
        Twitter.getTweet(r_id).then(tweet =>
            database.addSocialContent([tweet], req.params.idProduit).then(() => res.send(tweet))
        ).catch(e => res.status(500).send(e.stack));
    } else {
        res.send('error');
    }
});

app.get('/api/pin/:idContenuRef', (req, res) => {
    database.pinContenuRef(req.params.idContenuRef)
        .then(produit => res.send())
        .catch(e => res.status(500).send(e.stack));
});

app.get('/code', (req, res) => {
    database.searchProductBarcode(req.query.q)
        .then(produit => res.redirect('/vendeur?product_id='+produit.id))
        .catch(e => res.status(400).send(e.stack));
});
